package controllers
{
	import flash.events.Event;
	
	public class RadioButtonEvent extends Event
	{
		public static const SCORE:String = "score";
		
		public var score_array:Array;
		
		public function RadioButtonEvent(type:String, bubbles:Boolean=false, cancelable:Boolean=false)
		{
			super(type, bubbles, cancelable);
		}
		override public function clone():Event
		{
			return new RadioButtonEvent(type, bubbles, cancelable);
		}
	}
}