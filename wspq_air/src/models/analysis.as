package models
{
	public class analysis
	{	
		public function analysis()
		{
		}
		
		public static function analyse(p_in:int, e_in:int):String
		{
			var s:String = "Statist";
			var r:String = "Conservative";
			var l:String = "Liberal";
			var c:String = "Centrist";
			var b:String = "Libertarian";
				
			//Use array[row][column] to access political result
			var array:Array = new Array(10);
			
			array[0] = [s, s, s, s, s, r, r, r, r, r, r];
			array[1] = [s, s, s, s, s, r, r, r, r, r, r];
			array[2] = [s, s, s, s, s, c, r, r, r, r, r];
			array[3] = [s, s, s, s, c, c, c, r, r, r, r];
			array[4] = [s, s, s, c, c, c, c, c, r, r, r];
			array[5] = [l, l, c, c, c, c, c, c, c, r, r];
			array[6] = [l, l, l, c, c, c, c, c, b, b, b];
			array[7] = [l, l, l, l, c, c, c, b, b, b, b];
			array[8] = [l, l, l, l, l, c, b, b, b, b, b];
			array[9] = [l, l, l, l, l, l, b, b, b, b, b];
			array[10] = [l, l, l, l, l, l, b, b, b, b, b];
			
			var testString:String = p_in +','+ e_in;
			
			return array[p_in][e_in];
		}
		
		public static function get_details(user_politics:String):String
		{
			var details:String = "";
			
			switch (user_politics)
			{
				case "Statist":
					details = "Statists want government to have a great deal of power over the economy and individual behavior. " +
						"They frequently doubt whether economic liberty and individual freedom are practical options in today's world. " +
						"Statists tend to distrust the free market, support high taxes and centralized planning of the economy, oppose diverse lifestyles, " +
						"and question the importance of civil liberties.";
					break;
				case "Conservative":
					details = "Conservatives tend to favor economic freedom, but frequently support laws to restrict personal behavior that violates" +
						" 'traditional values.' They oppose excessive government control of business, while endorsing government action to defend morality " +
						"and the traditional family structure. Conservatives usually support a strong military, oppose bureaucracy and high taxes, " +
						"favor a free-market economy, and endorse strong law enforcement.";
					break;
				case "Liberal":
					details = "Liberals usually embrace freedom of choice in personal matters, but tend to support significant government control of the economy. " +
						"They generally support a government-funded 'safety net' to help the disadvantaged, and advocate strict regulation of business. " +
						"Liberals tend to favor environmental regulations, defend civil liberties and free expression, " +
						"support government action to promote equality, and tolerate diverse lifestyles.";
					break;
				case "Centrist":
					details = "Centrist prefer a 'middle ground' regarding government control of the economy and personal behavior. " +
						"Depending on the issue, they sometimes favor government intervention and sometimes support individual freedom of choice. " +
						"Centrists pride themselves on keeping an open mind, tend to oppose 'political extremes,' and emphasize what they describe as 'practical' " +
						"solutions to problems.";
					break;
				case "Libertarian":
					details = "" +
						"Libertarians support maximum liberty in both personal and economic matters. " +
						"They advocate a much smaller government; one that is limited to protecting individuals from coercion and violence. " +
						"Libertarians tend to embrace individual responsibility, oppose government bureaucracy and taxes, promote private charity, " +
						"tolerate diverse lifestyles, support the free market, and defend civil liberties.";
					break;
			}
			
			return details;
		}
	}
}
